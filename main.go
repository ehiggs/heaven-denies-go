package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	ginzerolog "github.com/easonlin404/gin-zerolog"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"github.com/rs/zerolog/log"
)

type PythonReleaseData struct {
	Name     string  `db:"name"`
	Version  string  `db:"version"`
	Filename string  `db:"filename"`
	File     []uint8 `db:"file"`
}

type PythonRelease struct {
	Name          string `db:"name"`
	Version       string `db:"version"`
	FileType      string `db:"file_type"`
	PythonVersion string `db:"python_version"`
	Filename      string `db:"filename"`
	Url           string `db:"url"`
	Size          int32  `db:"size"`
	UploadDate    int32  `db:"upload_date"`
	Sha256        string `db:"sha256"`
	Md5           string `db:"md5"`
	Blake2        string `db:"blake2_256"`
}

type PackageInfo struct {
	Author      string   `json:"author"`
	AuthorEmail *string  `json:"author_email"`
	BugtrackUrl *string  `json:"bugtrack_url"`
	Classifiers []string `json:"classifiers"`
	Description *string  `json:"description"`
	HomePage    *string  `json:"home_page"`
	License     *string  `json:"license"`
	Name        string   `json:"name"`
	ReleaseUrl  string   `json:"release_url"`
	// and other fields...
}

type PackageRelease struct {
	Url           string            `json:"url"`
	Filename      string            `json:"filename"`
	Digests       map[string]string `json:"digests"`
	Md5Digest     string            `json:"md5_digest"`
	PackageType   string            `json:"packagetype"`
	PythonVersion string            `json:"python_version"`
	Size          int32             `json:"size"`
}

type PackageUrl struct {
	Url      string            `json:"url"`
	Size     int32             `json:"size"`
	Yanked   bool              `json:"yanked"`
	Filename string            `json:"filename"`
	Digests  map[string]string `json:"digests"`
}

type GetProjectMetadataResponse struct {
	Info       PackageInfo                 `json:"info"`
	LastSerial int32                       `json:"last_serial"`
	Releases   map[string][]PackageRelease `json:"releases"`
	Urls       []PackageUrl                `json:"urls"`
}

type Server struct {
	host string
	repo *DBRepository
}

type GetRepository interface {
	GetReleases(name string) ([]PythonRelease, error)
	GetReleaseData(name, version, filename string) (PythonReleaseData, error)
}

type InsertRepository interface {
	InsertReleases(releases []PythonRelease) error
	InsertReleaseData(releaseData PythonReleaseData) error
}

type UpstreamRepository struct {
	db DBRepository
}

func getReleasesFromPackage(pkg GetProjectMetadataResponse) []PythonRelease {
	releases := make([]PythonRelease, 0, len(pkg.Releases))
	for version, releaseList := range pkg.Releases {
		for _, rel := range releaseList {
			release := PythonRelease{
				Name:          pkg.Info.Name,
				Version:       version,
				FileType:      rel.PackageType,
				PythonVersion: rel.PythonVersion,
				Filename:      rel.Filename,
				Url:           rel.Url,
				Size:          rel.Size,
				//UploadDate: rel.UploadDate,
				Sha256: rel.Digests["sha256"],
				Md5:    rel.Md5Digest,
			}
			releases = append(releases, release)
		}
	}
	return releases
}

func getReleasesFromUpstream(name string) (*GetProjectMetadataResponse, error) {
	url := fmt.Sprintf("https://pypi.org/pypi/%s/json", name)
	log.Printf("Accessing upstream url: %s", url)
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("Could not parse json for %s", name)
		return nil, err
	}
	if resp.StatusCode >= 400 {
		log.Printf("Could not retrieve %s, %d: ", name, resp.StatusCode)
		return nil, err
	}
	var pkg GetProjectMetadataResponse
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Could not parse json for %s", name)
		return nil, err
	}
	err = json.Unmarshal(body, &pkg)
	if err != nil {
		log.Printf("Parsing upstream json: ", err.Error())
		return nil, err
	}
	return &pkg, nil
}

func (s *UpstreamRepository) GetReleases(name string) ([]PythonRelease, error) {
	response, err := getReleasesFromUpstream(name)
	if err != nil {
		return nil, err
	}
	if response == nil {
		return nil, fmt.Errorf("could not find releases: %s", name)
	}
	releases := getReleasesFromPackage(*response)
	return releases, nil

}

func (s *UpstreamRepository) GetReleaseData(name, version, filename string) (PythonReleaseData, error) {
	releases, err := s.db.GetRelease(name, version, filename)
	if err != nil {
		log.Printf("Could not get release info:", err.Error())
		return PythonReleaseData{}, err
	}
	if len(releases) == 0 {
		log.Printf("Could not get upstream release info.")
		return PythonReleaseData{}, fmt.Errorf("could not get upstream release info: %s/%s/%s", name, version, filename)
	}
	url := fmt.Sprintf(releases[0].Url)
	log.Printf("Accessing upstream url: %s", url)
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("Could not parse json for %s", name)
		return PythonReleaseData{}, err
	}
	if resp.StatusCode >= 400 {
		log.Printf("Could not retrieve %s, %d: ", name, resp.StatusCode)
		return PythonReleaseData{}, err
	}
	log.Printf("Received upstream package data: %d, content length: %d", resp.StatusCode, resp.ContentLength)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Could not parse json for %s", name)
		return PythonReleaseData{}, err
	}

	return PythonReleaseData{Name: name, Version: version, Filename: filename, File: body}, nil
}

type DBRepository struct {
	db       sqlx.DB
	upstream GetRepository
}

func (s *DBRepository) GetReleases(name string) ([]PythonRelease, error) {
	var releases []PythonRelease
	err := s.db.Select(&releases, "SELECT * FROM python_releases WHERE name = :name", name)
	if err != nil {
		return nil, err
	}
	if len(releases) == 0 && s.upstream != nil {
		releases, err = s.upstream.GetReleases(name)
		if err != nil {
			log.Printf("Could not get upstream releases: ", err.Error())
			return nil, err
		}
		err = s.InsertReleases(releases)
		if err != nil {
			log.Printf("Could not insert upstream releases into db")
			return nil, err
		}
		log.Printf("Saved %d releases for %s", len(releases), name)
	}
	return releases, nil
}

func (s *DBRepository) GetRelease(name, version, filename string) ([]PythonRelease, error) {
	var releases []PythonRelease
	err := s.db.Select(&releases, `
	SELECT * FROM python_releases 
	WHERE name = $1 AND version = $2 AND filename = $3
	`, name, version, filename)
	if err != nil {
		return nil, err
	}
	if len(releases) == 0 && s.upstream != nil {
		releases, err := s.upstream.GetReleases(name)
		if err != nil {
			log.Printf("Could not get upstream releases: ", err.Error())
			return nil, err
		}
		s.InsertReleases(releases)
	}
	return releases, nil

}

func (s *DBRepository) GetReleaseData(name, version, filename string) (PythonReleaseData, error) {
	prepared, err := s.db.PrepareNamed(`
	SELECT * FROM python_release_data 
	WHERE name=:name AND version=:version AND filename=:filename
	`)
	if err != nil {
		log.Printf("Could not prepare statement to get release data: ", err.Error())
		return PythonReleaseData{}, err
	}
	args := map[string]interface{}{"name": name, "version": version, "filename": filename}
	var releaseDatas []PythonReleaseData
	err = prepared.Select(&releaseDatas, args)
	if err != nil {
		log.Printf("Could not run prepared statement: ", err.Error())
		return PythonReleaseData{}, err
	}

	if len(releaseDatas) == 0 && s.upstream != nil {
		releaseData, err := s.upstream.GetReleaseData(name, version, filename)
		if err != nil {
			log.Printf("Could not get upstream release data", err.Error())
			return PythonReleaseData{}, err
		}
		err = s.InsertReleaseData(releaseData)
		if err != nil {
			log.Printf("Could not insert upstream release data", err.Error())
			return PythonReleaseData{}, err
		}
		return releaseData, nil
	}
	return releaseDatas[0], nil
}

func (s *DBRepository) InsertReleases(releases []PythonRelease) error {

	query := `
	INSERT INTO python_releases 
	(name, version, file_type, python_version, filename, url, size, upload_date, sha256, md5, blake2_256) 
	VALUES 
	(:name, :version, :file_type, :python_version, :filename, :url, :size, :upload_date, :sha256, :md5, :blake2_256) 
	`
	_, err := s.db.NamedExec(query, releases)
	if err != nil {
		log.Printf("Could not insert python release info to db: ", err.Error())
	}
	return err
}

func (s *DBRepository) InsertReleaseData(releases PythonReleaseData) error {

	query := `
	INSERT INTO python_release_data
	(name, version, filename, file)
	VALUES 
	(:name, :version, :filename, :file)
	`
	_, err := s.db.NamedExec(query, releases)
	if err != nil {
		log.Printf("Could not insert python release info to db: ", err.Error())
	}
	return err
}

func (s *Server) GetPackageReleases(c *gin.Context) {
	name := c.Param("name")
	releases, err := s.repo.GetReleases(name)
	if err != nil {
		log.Printf("Could not access db: ", err.Error())
		c.JSON(500, gin.H{"message": "Could not access db"})
		return
	}
	log.Printf("Rendering %d releases for %s", len(releases), name)
	c.HTML(200, "releases.html", gin.H{"host": s.host, "name": name, "releases": releases})
}

func (s *Server) DownloadRelease(c *gin.Context) {
	name := c.Param("name")
	version := c.Param("version")
	filename := c.Param("filename")
	releaseData, err := s.repo.GetReleaseData(name, version, filename)
	if err != nil {
		log.Printf("Could not get release:", err.Error())
	}
	c.Data(200, "application/octet-stream", releaseData.File)
}

func createTables(db *sqlx.DB) error {
	_, err := db.Exec(`
	CREATE TABLE IF NOT EXISTS 	
	python_releases (
		name TEXT NOT NULL COLLATE NOCASE,
		version TEXT NOT NULL,
		file_type TEXT NOT NULL,
		python_version TEXT NOT NULL,
		filename TEXT NOT NULL,
		url TEXT NOT NULL,
		size INTEGER NOT NULL,
		upload_date INTEGER NOT NULL,
		sha256 TEXT NOT NULL,
		md5 TEXT NOT NULL,
		blake2_256 TEXT,
		PRIMARY KEY (name,version,filename));
	`)
	if err != nil {
		log.Printf("Coult not create table: ", err.Error())
		return err
	}
	_, err = db.Exec(`
	CREATE TABLE IF NOT EXISTS
	python_release_data (
		name TEXT NOT NULL COLLATE NOCASE, 
		version TEXT NOT NULL,
		filename TEXT NOT NULL,
		file BLOB NOT NULL, 
		PRIMARY KEY (name,version,filename));

	`)
	if err != nil {
		log.Printf("Coult not create table: ", err.Error())
		return err
	}
	return nil
}

func main() {
	// TOOD: config param
	db, err := sqlx.Open("sqlite3", "cache.db")
	if err != nil {
		fmt.Println("Could not load db", err.Error())
		return
	}
	err = createTables(db)
	if err != nil {
		log.Printf("Could not create database: ", err.Error())
		panic("bang")
	}

	// TOOD: config param
	//gin.SetMode(gin.ReleaseMode)

	noFallbackDBRepo := DBRepository{db: *db}
	repo := DBRepository{db: *db, upstream: &UpstreamRepository{db: noFallbackDBRepo}}
	s := Server{host: "localhost:8080", repo: &repo}
	r := gin.Default()
	r.Use(ginzerolog.Logger())
	r.LoadHTMLGlob("web/templates/*")
	r.GET("/repository/:name", s.GetPackageReleases)
	r.GET("/downloads/pypi/:name/:version/:filename", s.DownloadRelease)

	r.Run("localhost:8080")
}
